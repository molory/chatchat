//
//  ToastUtil.swift
//  ChatChat
//
//  Created by Marc OLORY on 01/07/2017.
//  Copyright © 2017 Razeware LLC. All rights reserved.
//

import UIKit
import Toast_Swift

class ToastUtils {
    
    static func showToast(_ message: String) {
        UIApplication.shared.keyWindow?.makeToast(message)
    }
    
    
    static func showLoading() {
        UIApplication.shared.keyWindow?.makeToastActivity(.center)

    }
    
    static func hideLoading() {
        UIApplication.shared.keyWindow?.hideToastActivity()
    }
    
}
