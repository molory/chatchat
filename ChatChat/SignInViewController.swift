//
//  SignInViewController.swift
//  ChatChat
//
//  Created by Marc OLORY on 20/06/2017.
//  Copyright © 2017 Razeware LLC. All rights reserved.
//

import UIKit

class SignInViewController : UIViewController {
    
    @IBOutlet weak var textFieldEmail: UITextField!
    
    @IBOutlet weak var textFieldPassword: UITextField!
    
    @IBOutlet weak var buttonConnect: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        textFieldEmail.placeholder = "Email"
        textFieldEmail.keyboardType = .emailAddress
        
        textFieldPassword.placeholder = "Password"
        textFieldPassword.isSecureTextEntry = true
    }
    
    @IBAction func onConnectClick(_ sender: Any) {
        let email = textFieldEmail.text ?? ""
        let password = textFieldPassword.text ?? ""
        
        ToastUtils.showLoading()
        SessionRepository.shared.signIn(email, password) { (session, error) in
            ToastUtils.hideLoading()
            if (error == nil) {
                self.proceedSignInSuccess()
            }else {
                self.proceedSignInError(error!)
            }
        }
        
    }
    
    private func proceedSignInSuccess() {
        performSegue(withIdentifier: "SignInToChannelList", sender: nil)
    }
    
    private func proceedSignInError(_ error : Error) {
        ToastUtils.showToast(error.localizedDescription)
    }
    
    
    
    
}
