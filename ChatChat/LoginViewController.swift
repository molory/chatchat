/*
* Copyright (c) 2015 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

import UIKit

class LoginViewController: UIViewController {
  
    @IBOutlet weak var nameField: UITextField!
    
    @IBOutlet weak var bottomLayoutGuideConstraint: NSLayoutConstraint!
  
    // MARK: View Lifecycle
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (SessionRepository.shared.isSessionOpen()) {
            performSegue(withIdentifier: "LoginToChatNoAnim", sender: nil)
        }
    }
  
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
  
    @IBAction func loginDidTouch(_ sender: AnyObject) {
    
        if nameField?.text != "" { // 1
            ToastUtils.showLoading()
            SessionRepository.shared.signInAnonymously() { (user, error) in
                ToastUtils.hideLoading()
                if let err = error { // 3
                    print(err.localizedDescription)
                    return
                }
                
                self.performSegue(withIdentifier: "LoginToChat", sender: nil) // 4
            }
        } else {
            ToastUtils.showToast("Name can not be empty")
        }
    }
  
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if (segue.identifier == "LoginToChat") {
            let navVc = segue.destination as! UINavigationController // 1
            let channelVc = navVc.viewControllers.first as! ChannelListViewController // 2
            channelVc.senderDisplayName = nameField?.text // 3
        }
    }
}

