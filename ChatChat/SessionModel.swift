//
//  SessionModel.swift
//  ChatChat
//
//  Created by Marc OLORY on 02/07/2017.
//  Copyright © 2017 Razeware LLC. All rights reserved.
//

import Foundation
import Firebase

class SessionModel {
    
    private let user : User
    
    var email : String? {
        return user.email
    }
    
    
    var isAnonymous : Bool {
        return user.isAnonymous
    }
    
    init(user : User) {
        self.user = user
    }
    
    
    
}
