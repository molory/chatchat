//
//  CreateAccountViewController.swift
//  ChatChat
//
//  Created by Marc OLORY on 20/06/2017.
//  Copyright © 2017 Razeware LLC. All rights reserved.
//

import UIKit

class CreateAccountViewController : UIViewController {
    
    @IBOutlet weak var textFieldEmail: UITextField!
    
    @IBOutlet weak var textFieldPassword: UITextField!
    
    @IBOutlet weak var buttonCreateAccount: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        textFieldEmail.placeholder = "Email"
        textFieldEmail.keyboardType = .emailAddress
        
        textFieldPassword.placeholder = "Password"
        textFieldPassword.isSecureTextEntry = true
    }
    
    @IBAction func onCreateAccountClick(_ sender: Any) {
        let email = textFieldEmail.text ?? ""
        let password = textFieldPassword.text ?? ""
        
        ToastUtils.showLoading()
        SessionRepository.shared.createUser(email, password) { (session, error) in
            ToastUtils.hideLoading()
            if (error == nil) {
                self.proceedCreateAccountSuccess()
            }else {
                self.proceedCreateAccoutError(error!)
            }
        }
    }
    
    private func proceedCreateAccountSuccess() {
        performSegue(withIdentifier: "CreateAccountToChannelList", sender: nil)
    }
    
    private func proceedCreateAccoutError(_ error : Error) {
        ToastUtils.showToast(error.localizedDescription)
    }
    
}
