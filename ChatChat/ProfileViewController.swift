//
//  ProfileViewController.swift
//  ChatChat
//
//  Created by Marc OLORY on 01/07/2017.
//  Copyright © 2017 Razeware LLC. All rights reserved.
//

import UIKit

class ProfileViewController : UIViewController {
    
    private static let ERROR_FIREBASE_NEED_LOGIN = 17014
    
    @IBOutlet weak var labelEmail: UILabel!
    
    @IBOutlet weak var buttonModifiyEmail: UIButton!
    
    @IBOutlet weak var buttonModifyPassword: UIButton!
    
    @IBOutlet weak var buttonLogout: UIButton!
    
    @IBOutlet weak var buttonModifyEmailHeightConstraint: NSLayoutConstraint!
    
    
    let sessionRepository = SessionRepository.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Profile"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let session = sessionRepository.getSession()
        let isAnonymous = session?.isAnonymous ?? true
        if (isAnonymous) {
            labelEmail.text = "You are anonymous"
        } else {
            labelEmail.text = session?.email
        }
        
        buttonModifyPassword.isHidden = isAnonymous
        buttonModifiyEmail.isHidden = isAnonymous
        buttonModifyEmailHeightConstraint.constant = isAnonymous ? 0 : 50
    }
    
    
    @IBAction func onModifyEmailClick(_ sender: Any) {
        showAskForNewEmail()
    }
    
    @IBAction func onModifyPasswordClick(_ sender: Any) {
        showAskForNewPassword()
    }
    
    @IBAction func onLogoutclick(_ sender: Any) {
        sessionRepository.logout()
        dismiss(animated: true) {
            ToastUtils.showToast("You are disconnected")
        }
    }
    
    private func currentUserEmail() -> String {
        return sessionRepository.getSession()?.email ?? ""
    }
    
    private func showAskForNewEmail() {
        
        let alert = UIAlertController(title: nil, message: "Edit email", preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.text = self.currentUserEmail()
        }
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert!.textFields![0]
            self.proceedEditEmail(textField.text)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { [weak alert] (_) in
            alert?.dismiss(animated: true)
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    private func showAskForNewPassword() {
        
        let alert = UIAlertController(title: nil, message: "Edit password", preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.placeholder = "New password"
            textField.isSecureTextEntry = true
        }
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert!.textFields![0]
            self.proceedEditPassword(textField.text)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { [weak alert] (_) in
            alert?.dismiss(animated: true)
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    private func proceedEditPassword(_ password : String?) {
        if (password == nil || password!.isBlank) {
            ToastUtils.showToast("Password can not be empty")
        } else {
            ToastUtils.showLoading()
            sessionRepository.updatePassword(password!) { (_, error) in
                ToastUtils.hideLoading()
                if ( error == nil) {
                    ToastUtils.showToast("Password updated successfully")
                } else {
                    let errorCode = (error! as NSError).code
                    
                    if (errorCode == ProfileViewController.ERROR_FIREBASE_NEED_LOGIN) {
                        self.proceedRequiresRecentLoginError() {
                            self.proceedEditPassword(password)
                        }
                    } else {
                        ToastUtils.showToast(error!.localizedDescription)
                    }
                }
            }
        }
    }
    
    private func proceedEditEmail(_ email : String?) {
        if (email == nil || email!.isBlank) {
            ToastUtils.showToast("Email can not be empty")

        } else {
            ToastUtils.showLoading()
            sessionRepository.updateEmail(email!) { (_, error) in
                ToastUtils.hideLoading()
                if ( error == nil) {
                    self.labelEmail.text = email
                    ToastUtils.showToast("Email updated successfully")
                } else {
                    let errorCode = (error! as NSError).code
                    
                    if (errorCode == ProfileViewController.ERROR_FIREBASE_NEED_LOGIN) {
                        self.proceedRequiresRecentLoginError() {
                            self.proceedEditEmail(email)
                        }
                    } else {
                        ToastUtils.showToast(error!.localizedDescription)
                    }
                }
            }
        }
        
    }
    
    private func proceedRequiresRecentLoginError(afterLogin : @escaping () -> Void) {
        let alert = UIAlertController(title: nil, message: "Enter your password", preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.placeholder = "Password"
            textField.isSecureTextEntry = true
        }
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textFieldPassword = alert!.textFields![0]
            self.proceedLoginRefresh(self.currentUserEmail(), textFieldPassword.text, afterLogin)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { [weak alert] (_) in
            alert?.dismiss(animated: true)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func proceedLoginRefresh(_ email : String?, _ password : String?, _ afterLogin : @escaping () -> Void) {
        
        if (email == nil || email!.isBlank) {
            ToastUtils.showToast("Email can not be empty")
        }
        
        if (password == nil || password!.isBlank) {
            ToastUtils.showToast("Email can not be empty")
        }
        ToastUtils.showLoading()
        
        sessionRepository.signIn(email!, password!) { (session, error) in
            ToastUtils.hideLoading()
            if ( error == nil) {
                afterLogin()
            } else {
                ToastUtils.showToast(error!.localizedDescription)
            }
        }
        
    }
    
    
    
    
}
