//
//  String+Util.swift
//  ChatChat
//
//  Created by Marc OLORY on 01/07/2017.
//  Copyright © 2017 Razeware LLC. All rights reserved.
//

import Foundation

extension String {
    
    var  isBlank:Bool {
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).isEmpty
    }
}
