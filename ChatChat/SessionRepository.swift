//
//  SessionRepository.swift
//  ChatChat
//
//  Created by Marc OLORY on 02/07/2017.
//  Copyright © 2017 Razeware LLC. All rights reserved.
//

import Foundation
import FirebaseAuth

class SessionRepository {
    static let shared = SessionRepository()

    
    func isSessionOpen() -> Bool {
        return Auth.auth().currentUser != nil
    }
    
    func logout() {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    func updatePassword(_ password: String, _ callback : @escaping RequestorCallback) {
        Auth.auth().currentUser?.updatePassword(to: password) { (error) in
            callback(nil, error as NSError?)
        }
    }
    
    func updateEmail(_ email : String, _ callback : @escaping RequestorCallback) {
        Auth.auth().currentUser?.updateEmail(to: email) { (error) in
            callback(nil, error as NSError?)
        }
    }
    
    func signInAnonymously(_ callback : @escaping RequestorCallback) {
        Auth.auth().signInAnonymously(completion: { (user, error) in
            callback(user as Any, error as NSError?)
        })
    }
    
    func signIn(_ email : String, _ password : String, _ callback : @escaping RequestorCallback) {
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            callback(self.getSession(), error as NSError?)
        }
    }
    
    func createUser(_ email : String, _ password : String, _ callback : @escaping RequestorCallback) {
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            callback(self.getSession(), error as NSError?)
        }
    }
    
    func getSession() -> SessionModel? {
        let user = Auth.auth().currentUser
        if (user == nil) {
            return nil
        } else {
            return SessionModel(user: user!)
        }
    }
    
}
